Name: PATRICIA ANUGRAH

NPM: 1706074884

Class: ADVANCE PROGRAMMING-B

Hobby: LISTENING TO MUSIC, WATCHING MOVIES, SINGING

# Links

git exercise link: https://gitlab.com/patricia.anugrah/my-first-git-repo

Javari Park Festival link: https://gitlab.com/patricia.anugrah/TugasPemrograman_DDP2/tree/master/assignment-3

# My Notes

## Git Branches Usage:
Kegunaan Git Branches adalah ketika kita butuh meng-edit sesuatu tapi kita masih kurang yakin apakah editan tersebut dapat berjalan atau tidak, Git Branch berguna untuk proyek kita. Sebab, ketika kita meng-edit sesuatu di branch, files kita yang di master tidak berubah.

Saya menggunakan beberapa perintah untuk menggunakan Git Branches di repository. Pertama, saya memastikan bahwa saya ada di master branch. Selanjutnya, saya membuat branch baru dengan perintah git branch <BRANCHNAME> (atau git branch cool-feature). Lalu, saya menukar branch ke yang baru dengan git checkout cool-feature. Lalu, saya menambahkan beberapa fitur di README.md saya lalu saya add, commit, push dengan git push -u origin cool-feature. Setelahnya, saya kembali ke master branch dengan git checkout master lalu saya menambahkan satu line baru di paling bawah dari README.md dan setelahnya saya push kembali ke Git. Seterusnya, saya menggunakan perintah git merge cool-feature dan terjadi konflik sehingga saya harus mengubah README.md tersebut dengan beberapa cara, termasuk mengubah hal yang membuat konflik tersebut.

## Git Revert Usage:
Kegunaan Git Revert adalah ketika kita membuat perubahan pada source code di mana code tersebut seharusnya tidak perlu di-commit. Untuk membatalkan perubahan tersebut, Git Revert digunakan. Git Revert membuat kita dapat kembali ke perubahan apapun pada source code dengan mengembalikan file kepada kondisi semula.

Seperti pada Git Branches, saya mengikuti step di tutorial 0 untuk mengerjakan Git Revert. Pertama, saya mengetik git status untuk melihat kondisi working directory dan staging area. Selanjutnya, untuk me-list files yang terdapat di repository, saya mengetik perintah ls. Untuk melihat apa isi file-nya, saya menggunakan perintah cat README.md. Setelahnya, dengan perintah git log, terdapat list dari commits pada repository dengan urutan dari commit terbaru. Saya dapat mengambil commit apapun yang saya inginkan untuk kembali. Saya dapat menggunakan perintah git checkout <commit hash> untuk melihat kondisi file saya seperti apa pada commit tersebut. Lalu, saya kembali ke git revert dengan mengetik perintah git checkout master untuk kembali ke commit terakhir dari branch master. Selanjutnya, saya menggunakan perintah git revert <commit hash> untuk kembalikan commit yang saya inginkan. Ketika ada konflik pada git revert, saya perbaiki README.md saya. Terakhir, saya push kembali dan sehingga pada perintah git log terdapat tylisan This reverts commit <commit hash> pada commit paling atas.

